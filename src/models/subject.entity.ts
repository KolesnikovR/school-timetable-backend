import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { User } from 'src/models/user.entity';

@Entity()
export class Subject {
    @PrimaryGeneratedColumn({
        name: "subject_id",
        type: "int",
    })
	id: number;

    @Column({
        type: "varchar",
        name: "subject_name",
        nullable: false,
        unique: true,
    })
    name: string;

    @Column({
        type: "int",
        name: "difficulty",
        nullable: false,
    })
    difficulty: number;

    @ManyToMany(() => User, teacher => teacher.subjects)
    @JoinTable({
        name: 'subject_teacher',
        joinColumn: {
            name: "subject_id",
        },
        inverseJoinColumn: {
            name: "teacher_id",
        }
    })
    teachers: User[];
}
