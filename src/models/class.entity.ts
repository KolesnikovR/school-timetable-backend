import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Lesson } from 'src/models/lesson.entity';

@Entity()
export class Class {
    @PrimaryGeneratedColumn({
        name: "class_id",
        type: "int",
    })
	id: number;

    @Column({
        name: "class_name",
        type: "varchar",
        nullable: false, 
        unique: true,
    })
    name: string;

    @OneToMany(type => Lesson, lesson => lesson.classData, )
    lessons: Lesson[];
}
