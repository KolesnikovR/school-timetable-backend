import { Entity, Column, ManyToOne, PrimaryGeneratedColumn, JoinColumn, BeforeInsert, BeforeUpdate, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { Role } from 'src/models/role.entity';
import { Subject } from 'src/models/subject.entity';
import { hashPassword } from 'src/shared/hashPassword';
import { Lesson } from 'src/models/lesson.entity';

@Entity()
export class User {
    @PrimaryGeneratedColumn({
        name: "user_id",
        type: "int",
    })
	id: number;

    @Column({
        type: "varchar",
        nullable: false,
    })
    name: string;

    @Column({
        type: "varchar",
        nullable: false,
    })
    surname: string;

    @Column({
        type: "varchar",
        nullable: false,
    })
    patronymic: string;

    @Column({
        type: "varchar",
        nullable: false,
        unique: true,
    })
    email: string;

    @Column({
        type: "varchar",
        nullable: false,
        select: false,
    })
    password: string;

    @Column({
        type: "varchar",
    })
    phone: string;

    @ManyToOne(type => Role, role => role.id, {
        onDelete: "CASCADE",
    })
    @JoinColumn({ name: "role_id" })
    role: Role;

    @ManyToMany(() => Subject, (subject: Subject) => subject.teachers)
    subjects: Subject[];

    @OneToMany(type => Lesson, lesson => lesson.teacher, )
    lessons: Lesson[];

    @BeforeInsert()
    async hashPassword() {
        this.password = await hashPassword(this.password);
    }
}
