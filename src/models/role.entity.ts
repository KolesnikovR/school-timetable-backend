import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Role {
    @PrimaryGeneratedColumn({
        name: "role_id",
        type: "int",
    })
	id: number;

    @Column({
        name: "role_name",
        type: "varchar",
        nullable: false, 
        unique: true,
    })
    name: string;
}
