import { Entity, Column, ManyToOne, PrimaryGeneratedColumn, JoinColumn } from 'typeorm';
import { User } from 'src/models/user.entity';
import { Subject } from 'src/models/subject.entity';
import { Class } from 'src/models/class.entity';

@Entity()
export class Lesson {
    @PrimaryGeneratedColumn({
        name: "lesson_id",
        type: "int",
    })
	id: number;

    @Column({
        type: "int",
        name: "count_per_week",
        nullable: false,
    })
    countPerWeek: number;

    @ManyToOne(type => Class, classData => classData.id, {
        onDelete: "CASCADE",
    })
    @JoinColumn({ name: "class_id" })
    classData: Class;

    @ManyToOne(type => Subject, subject => subject.id, {
        onDelete: "CASCADE",
    })
    @JoinColumn({ name: "subject_id" })
    subject: Subject;

    @ManyToOne(type => User, user => user.id, {
        onDelete: "CASCADE",
    })
    @JoinColumn({ name: "teacher_id" })
    teacher: User;
}
