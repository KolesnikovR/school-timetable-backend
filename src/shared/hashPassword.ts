import { hash } from "bcrypt";

const USER_PASSWORD_SALT = 10;

export const hashPassword = async (password: string): Promise<string> =>
    await hash(password, USER_PASSWORD_SALT);
