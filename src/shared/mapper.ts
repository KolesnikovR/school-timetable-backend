import { User } from "src/models/user.entity";
import { UserDto } from "src/modules/users/users.dto";

export const toUserDto = (data: User): UserDto => {
    const { id, name, surname, patronymic, email, phone, role, subjects, lessons } = data;
    let userDto: UserDto = { id, name, surname, patronymic, email, phone, role, subjects, lessons };
    return userDto;
}
