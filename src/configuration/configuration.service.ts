import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { MailerOptions } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

require('dotenv').config();

export class ConfigurationService {
	constructor(private env: { [k: string]: string | undefined }) {}

	private getValue(key: string, throwOnMissing = true): string {
		const value = this.env[key];
		if (!value && throwOnMissing) {
			throw new Error(`Config error - missing env.${key}`);
		}

		return value;
	}

	public ensureValues(keys: string[]) {
		keys.forEach((k) => this.getValue(k, true));
		return this;
	}

	public getPort() {
		return this.getValue('PORT', true);
	}

	public getFrontEndLink() {
		return this.getValue('FRONTEND_APP_LINK', true);
	}

	public getSecret() {
		return this.getValue('APP_SECRET', true);
	}

	public getTokenLifetime() {
		return this.getValue('TOKEN_EXPIRES_IN', true);
	}

	public isProduction() {
		const mode = this.getValue('APP_MODE', false);
		return mode !== 'DEV';
	}

	public getTypeOrmConfig(): TypeOrmModuleOptions {
		return {
			type: 'postgres',

			host: this.getValue('DB_HOST'),
			port: parseInt(this.getValue('DB_PORT')),
			username: this.getValue('DB_USER'),
			password: this.getValue('DB_PASSWORD'),
			database: this.getValue('DB_DATABASE'),

			entities: ['dist/**/*.entity{.ts,.js}'],

			migrationsTableName: 'migration',

			migrations: ['dist/migration/*{.ts,.js}'],

			cli: {
				migrationsDir: 'src/migration',
			},

			ssl: true,
		};
	}

	public getSmtpConfig(): MailerOptions {
		return {
			transport: {
				host: this.getValue('SMTP_HOST'),
				port: this.getValue('SMTP_PORT'),
				secure: false,
				auth: {
					user: this.getValue('SMTP_USER'),
					pass: this.getValue('SMTP_PASSWORD'),
				},
			},
			defaults: {
				from: this.getValue('SMTP_USER'),
			},
			template: {
				dir: process.cwd() + '/templates/',
				adapter: new HandlebarsAdapter(),
				options: {
					strict: true,
				},
			}
		};
	}
}

const configService = new ConfigurationService(process.env).ensureValues([
	'DB_HOST',
	'DB_PORT',
	'DB_USER',
	'DB_PASSWORD',
	'DB_DATABASE',
]);

export { configService };
