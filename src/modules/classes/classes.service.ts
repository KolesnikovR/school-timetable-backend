import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { Class } from 'src/models/class.entity';
import { CreateClassDto, ClassDto, UpdateClassDto } from 'src/modules/classes/classes.dto';

const RELATIONS = ['lessons', 'lessons.subject', 'lessons.teacher'];

@Injectable()
export class ClassesService {
    constructor(
        @InjectRepository(Class)
        private readonly classRepository: Repository<Class>,
    ) {}

    async create(classDto: CreateClassDto): Promise<ClassDto> {
        const classInDB = await this.classRepository.findOne({ where: { name: classDto.name } });

        if (classInDB) {
            throw new HttpException('Класс с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        return await this.classRepository.save(classDto);
    }

    async findAll(condition?: FindManyOptions<Class>): Promise<Class[]> {
        const finalOptions = Object.assign(
            { relations: RELATIONS },
            condition,
        );
        return await this.classRepository.find(finalOptions);
    }

    async findOne(options?: object): Promise<ClassDto> {
        const finalOptions = Object.assign(
            { relations: RELATIONS },
            options || {},
        );
        const subject = await this.classRepository.findOne(finalOptions);

        if (!subject) {
            throw new HttpException('Класс не найден', HttpStatus.NOT_FOUND);
        }

        return subject;
    }

    async remove(id: number): Promise<void> {
        await this.classRepository.delete(id);
    }

    async update(id: number, classDto: UpdateClassDto) {
        if (classDto.name) {
            const classInDB = await this.classRepository.findOne({ where: { name: classDto.name } });

            if (classInDB) {
                throw new HttpException('Класс с таким именем уже существует', HttpStatus.BAD_REQUEST);
            }    
        }

        await this.classRepository.update(id, classDto);
        return this.findOne({ where: { id } });
    }
}
