import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/guards/roles.guard';
import { ClassesService } from 'src/modules/classes/classes.service';
import { CreateClassDto, ClassDto, UpdateClassDto } from 'src/modules/classes/classes.dto';

@Controller('classes')
export class ClassesController {
    constructor(private classesService: ClassesService) {}

    @Get()
    findAll() {
        try {
            return this.classesService.findAll();
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Get(':id')
    findById(@Param('id') id: number) {
        try {
            return this.classesService.findOne({ where: { id } });
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Put(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    public async updateClassById(@Body() updateClassDto: UpdateClassDto, @Param('id') id: number) {
        try {
            return this.classesService.update(id, updateClassDto);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Post()
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Roles('admin')
    public async create(@Body() createClassDto: CreateClassDto): Promise<ClassDto> {
        try {
            return await this.classesService.create(createClassDto);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Delete(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    deleteOne(@Param('id') id: number) {
        try {
            return this.classesService.remove(id);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }
}
