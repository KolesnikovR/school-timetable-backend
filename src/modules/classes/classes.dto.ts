import {
    IsNotEmpty,
    IsString,
    IsOptional,
    IsNumber,
} from 'class-validator';
import { Lesson } from 'src/models/lesson.entity';

export class CreateClassDto {  
    @IsNotEmpty() @IsString() name: string;
}

export class UpdateClassDto {
    @IsOptional() @IsString() name?: string;
}

export class ClassDto {
    @IsNotEmpty() @IsNumber() id: number;
    @IsNotEmpty() @IsString() name: string;    
    @IsNotEmpty() lessons: Lesson[];
}
