import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { Class } from 'src/models/class.entity';
import { ClassesService } from './classes.service';
import { ClassesController } from './classes.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([Class]),
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false,
        }),
    ],
    providers: [ClassesService],
    controllers: [ClassesController],
    exports: [ClassesService],
})
export class ClassesModule {}
