import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { Subject } from 'src/models/subject.entity';
import { UsersService } from 'src/modules/users/users.service';
import { CreateSubjectDto, SubjectDto, UpdateSubjectDto } from 'src/modules/subjects/subjects.dto';
import { User } from 'src/models/user.entity';

const RELATIONS = ['teachers'];

@Injectable()
export class SubjectService {
    constructor(
        @Inject(forwardRef(() => UsersService))
        private readonly usersService: UsersService,
        @InjectRepository(Subject)
        private readonly subjectsRepository: Repository<Subject>,
    ) {}

    async create(subjectDto: CreateSubjectDto): Promise<SubjectDto> {
        const subjectInDB = await this.subjectsRepository.findOne({ where: { name: subjectDto.name } });

        if (subjectInDB) {
            throw new HttpException('Предмет с таким именем уже существует', HttpStatus.BAD_REQUEST);
        }

        const { teachers: teachersIds, ...subjectData } = subjectDto;
        const teachers = teachersIds.length ? await this.usersService.findAll({
            where: teachersIds.map(id => ({ id }))
        }) : [];

        const subject: Subject = this.subjectsRepository.create({
            ...subjectData,
            teachers,
        });

        await this.subjectsRepository.save(subject);
        return subject;
    }

    async findAll(condition?: FindManyOptions<Subject>): Promise<Subject[]> {
        const findOptions = Object.assign({ relations: RELATIONS, }, condition || {});
        return await this.subjectsRepository.find(findOptions);
    }

    async findOne(options?: object): Promise<SubjectDto> {
        const finalOptions = Object.assign({ relations: RELATIONS }, options || {});
        const subject = await this.subjectsRepository.findOne(finalOptions);

        if (!subject) {
            throw new HttpException('Предмет не найдет', HttpStatus.NOT_FOUND);
        }

        return subject;
    }

    async remove(id: number): Promise<void> {
        await this.subjectsRepository.delete(id);
    }

    async update(id: number, subjectDto: UpdateSubjectDto) {
        let additionalProperties: { teachers?: User[] } = {};
        const { teachers: teachersIds, ...subjectData } = subjectDto;

        if (subjectData.name) {
            const subjectInDB = await this.subjectsRepository.findOne({ where: { name: subjectData.name } });

            if (subjectInDB) {
                throw new HttpException('Предмет с таким именем уже существует', HttpStatus.BAD_REQUEST);
            }    
        }
    
        if (teachersIds) {
            additionalProperties.teachers = teachersIds.length ? await this.usersService.findAll({
                where: teachersIds.map(id => ({ id })),
                relations: ['role'],
            }) : [];
        }

        let oldSubject = await this.findOne({ where: { id } });
        const newSubject = Object.assign(oldSubject, subjectData, additionalProperties);
        await this.subjectsRepository.save(newSubject);
        return await this.findOne({ where: { id } });
    }
}
