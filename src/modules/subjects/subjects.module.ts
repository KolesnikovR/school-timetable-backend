import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubjectService } from 'src/modules/subjects/subjects.service';
import { SubjectsController } from 'src/modules/subjects/subjects.controller';
import { PassportModule } from '@nestjs/passport';
import { Subject } from 'src/models/subject.entity';
import { UsersModule } from 'src/modules/users/users.module';

@Module({
    imports: [
        forwardRef(() => UsersModule),
        TypeOrmModule.forFeature([Subject]),
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false,
        }),
    ],
    providers: [SubjectService],
    controllers: [SubjectsController],
    exports: [SubjectService],
})
export class SubjectsModule {}
