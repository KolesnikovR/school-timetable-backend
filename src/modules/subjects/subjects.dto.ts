import {
    IsNotEmpty,
    IsString,
    IsOptional,
    IsArray,
    ArrayUnique,
    IsNumber,
} from 'class-validator';
import { User } from 'src/models/user.entity';

export class CreateSubjectDto {  
    @IsNotEmpty() @IsString() name: string;
    @IsNotEmpty() @IsNumber() difficulty: number;
    @IsArray() @ArrayUnique() teachers: number[];
}

export class UpdateSubjectDto {
    @IsOptional() @IsString() name?: string;
    @IsOptional() @IsNumber() difficulty?: number;
    @IsOptional() @IsArray() @ArrayUnique() teachers?: number[];
}

export class SubjectDto {
    @IsNotEmpty() @IsNumber() id: number;
    @IsNotEmpty() @IsString() name: string;
    @IsNotEmpty() @IsNumber() difficulty: number;
    @IsArray() teachers: User[];
}
