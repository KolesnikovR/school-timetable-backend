import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/guards/roles.guard';
import { SubjectService } from 'src/modules/subjects/subjects.service';
import { CreateSubjectDto, UpdateSubjectDto } from './subjects.dto';

@Controller('subjects')
export class SubjectsController {
    constructor(private subjectsService: SubjectService) {}

    @Get()
    findAll() {
        try {
            return this.subjectsService.findAll();
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Get(':id')
    findById(@Param('id') id: number) {
        try {
            return this.subjectsService.findOne({ where: { id } });
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Put(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    public async updateSubjectById(@Body() updateSubjectDto: UpdateSubjectDto, @Param('id') id: number) {
        try {
            return this.subjectsService.update(id, updateSubjectDto);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Post()
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Roles('admin')
    public async create(@Body() createsubjectDto: CreateSubjectDto) {
        try {
            return await this.subjectsService.create(createsubjectDto);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Delete(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    deleteOne(@Param('id') id: number) {
        try {
            return this.subjectsService.remove(id);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }
}
