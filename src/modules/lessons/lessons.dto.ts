import {
    IsNotEmpty,
    IsOptional,
    IsNumber,
    IsPositive,
} from 'class-validator';
import { Class } from 'src/models/class.entity';
import { Subject } from 'src/models/subject.entity';
import { User } from 'src/models/user.entity';

export class CreateLessonDto {  
    @IsNotEmpty() @IsNumber() @IsPositive() countPerWeek: number;
    @IsNotEmpty() @IsNumber() classData: number;
    @IsNotEmpty() @IsNumber() subject: number;
    @IsNotEmpty() @IsNumber() teacher: number;
}

export class UpdateLessonDto {
    @IsOptional() @IsNumber() @IsPositive() countPerWeek?: number;
    @IsOptional() @IsNumber() classData?: number;
    @IsOptional() @IsNumber() subject?: number;
    @IsOptional() @IsNumber() teacher?: number;
}

export class LessonDto {
    @IsNotEmpty() @IsNumber() id: number;
    @IsNotEmpty() @IsNumber() @IsPositive() countPerWeek: number;
    @IsNotEmpty() teacher: User;
    @IsNotEmpty() classData: Class;
    @IsNotEmpty() subject: Subject;
}
