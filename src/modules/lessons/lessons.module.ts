import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { LessonsService } from 'src/modules/lessons/lessons.service';
import { LessonsController } from 'src/modules/lessons/lessons.controller';
import { UsersModule } from 'src/modules/users/users.module';
import { SubjectsModule } from 'src/modules/subjects/subjects.module';
import { ClassesModule } from 'src/modules/classes/classes.module';
import { Lesson } from 'src/models/lesson.entity';

@Module({
    imports: [
        UsersModule,
        SubjectsModule,
        ClassesModule,
        TypeOrmModule.forFeature([Lesson]),
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false,
        }),
    ],
    providers: [LessonsService],
    controllers: [LessonsController],
    exports: [LessonsService],
})
export class LessonsModule {}
