import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { CreateLessonDto, LessonDto, UpdateLessonDto } from 'src/modules/lessons/lessons.dto';
import { Lesson } from 'src/models/lesson.entity';
import { UsersService } from 'src/modules/users/users.service';
import { SubjectService } from 'src/modules/subjects/subjects.service';
import { ClassesService } from 'src/modules/classes/classes.service';
import { Subject } from 'src/models/subject.entity';
import { Class } from 'src/models/class.entity';
import { UserDto } from 'src/modules/users/users.dto';

const RELATIONS = ['subject', 'teacher', 'classData'];

@Injectable()
export class LessonsService {
    constructor(
        private readonly usersService: UsersService,
        private readonly subjectsService: SubjectService,
        private readonly classesService: ClassesService,
        @InjectRepository(Lesson)
        private readonly lessonRepository: Repository<Lesson>,
    ) {}

    async create(lessonDto: CreateLessonDto): Promise<LessonDto> {
        const lessonInDB = await this.lessonRepository.findOne({
            where: {
                subject: { id: lessonDto.subject },
                classData: { id: lessonDto.classData },
            } 
        });

        if (lessonInDB) {
            throw new HttpException('Урок уже существует', HttpStatus.BAD_REQUEST);
        }

        const { teacher: teacherId, classData: classId, subject: subjectId, ...lessonData } = lessonDto;
        const teacher = await this.usersService.findOne({ where: { id: teacherId } });

        if (!teacher.subjects.filter(subj => subj.id === subjectId).length) {
            throw new HttpException('Учитель не может создавать урок', HttpStatus.BAD_REQUEST);
        }

        const subject = await this.subjectsService.findOne({ where: { id: subjectId } });
        const classData = await this.classesService.findOne({ where: { id: classId } });

        const lesson: Lesson = this.lessonRepository.create({
            ...lessonData,
            teacher,
            subject,
            classData
        });

        await this.lessonRepository.save(lesson);
        return lesson;
    }

    async findAll(condition?: FindManyOptions<Lesson>): Promise<Lesson[]> {
        const findOptions = Object.assign({ relations: RELATIONS, }, condition || {});
        return await this.lessonRepository.find(findOptions);
    }

    async findOne(options?: object): Promise<LessonDto> {
        const findOptions = Object.assign({ relations: RELATIONS, }, options || {});
        const lesson = await this.lessonRepository.findOne(findOptions);

        if (!lesson) {
            throw new HttpException('Урок не найден', HttpStatus.NOT_FOUND);
        }

        return lesson;
    }

    async remove(id: number): Promise<void> {
        await this.lessonRepository.delete(id);
    }

    async update(id: number, lessonDto: UpdateLessonDto) {
        let additionalProperties: { teacher?: UserDto, subject?: Subject, classData?: Class } = {};

        if (lessonDto.classData && lessonDto.subject) {
            const lessonInDB = await this.lessonRepository.findOne({
                where: {
                    subject: { id: lessonDto.subject },
                    classData: { id: lessonDto.classData },
                } 
            });
    
            if (lessonInDB) {
                throw new HttpException('Урок уже создан', HttpStatus.BAD_REQUEST);
            }  
        }

        const { teacher: teacherId, classData: classId, subject: subjectId, ...lessonData } = lessonDto;
        let oldLesson = await this.findOne({ where: { id } });

        if (teacherId) {
            const teacher = await this.usersService.findOne({ where: { id: teacherId } });

            if (!teacher.subjects.filter(subj => subj.id === (subjectId || oldLesson.subject.id)).length) {
                throw new HttpException('Учитель не может создавать урок', HttpStatus.BAD_REQUEST);
            }
            
            additionalProperties.teacher = teacher;
        }
        
        if (subjectId) {
            additionalProperties.subject = await this.subjectsService.findOne({ where: { id: subjectId } });
        }

        if (classId) {
            additionalProperties.classData = await this.classesService.findOne({ where: { id: classId } });
        }

        const newLesson = Object.assign(oldLesson, lessonData, additionalProperties);
        await this.lessonRepository.save(newLesson);
        return this.findOne({ whete: { id } });
    }
}
