import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/guards/roles.guard';
import { LessonsService } from 'src/modules/lessons/lessons.service';
import { CreateLessonDto, LessonDto, UpdateLessonDto } from 'src/modules/lessons/lessons.dto';

@Controller('lessons')
export class LessonsController {
    constructor(private lessonsService: LessonsService) {}

    @Get()
    public async findAll() {
        try {
            return await this.lessonsService.findAll();
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Get(':id')
    public async findById(@Param('id') id: number) {
        try {
            return await this.lessonsService.findOne({ where: { id } });
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Put(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    public async updateUserById(@Body() updateLessonDto: UpdateLessonDto, @Param('id') id: number) {
        try {
            return await this.lessonsService.update(id, updateLessonDto);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Post()
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Roles('admin')
    public async create(@Body() createClassDto: CreateLessonDto): Promise<LessonDto> {
        try {
            return await this.lessonsService.create(createClassDto);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }

    @Delete(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    public async deleteOne(@Param('id') id: number) {
        try {
            return await this.lessonsService.remove(id);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }
}
