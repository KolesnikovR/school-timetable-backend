import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from 'src/modules/users/users.service';
import { UsersController } from 'src/modules/users/users.controller';
import { User } from 'src/models/user.entity';
import { Role } from 'src/models/role.entity';
import { SubjectsModule } from 'src/modules/subjects/subjects.module';
import { PassportModule } from '@nestjs/passport';

@Module({
    imports: [
        forwardRef(() => SubjectsModule),
        TypeOrmModule.forFeature([User, Role]),
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false,
        }),
    ],
    providers: [UsersService],
    controllers: [UsersController],
    exports: [UsersService],
})
export class UsersModule {}
