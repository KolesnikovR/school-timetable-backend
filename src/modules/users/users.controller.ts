import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Put, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/guards/roles.guard';
import { UsersService } from 'src/modules/users/users.service';
import { UpdateUserDto, UserDto } from 'src/modules/users/users.dto';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) {}

    @Get()
    findAll() {
        return this.usersService.findAll();
    }

    @Get(':id')
    findById(@Param('id') id: number) {
        return this.usersService.findOne({ where: { id } });
    }

    @Patch('/password')
    @UseGuards(AuthGuard('jwt'))
    public async updatePassword(@Body() { password }: UpdateUserDto, @Req() request): Promise<void> {
        if (!password) {
            throw new HttpException('Password property is neccesary', HttpStatus.BAD_REQUEST);    
        }

        this.usersService.update(request.user.id, { password });
    }

    @Put(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    public async updateUserById(@Body() updateUserDto: UpdateUserDto, @Param('id') id: number): Promise<UserDto> {
        return this.usersService.update(id, updateUserDto);
    }

    @Put()
    @UseGuards(AuthGuard())
    public async updateUser(@Body() updateUserDto: UpdateUserDto, @Req() request): Promise<UserDto> {
        const { role, ...newUserData } = updateUserDto;
        return this.usersService.update(request.user.id, newUserData);
    }

    @Delete(':id')
    @UseGuards(AuthGuard(), RolesGuard)
    @Roles('admin')
    deleteOne(@Param('id') id: number) {
        return this.usersService.remove(id);
    }
}
