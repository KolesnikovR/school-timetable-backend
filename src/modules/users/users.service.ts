import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { compare } from 'bcrypt';
import { FindManyOptions, Repository } from 'typeorm';
import { User } from 'src/models/user.entity';
import { toUserDto } from 'src/shared/mapper';
import { CreateUserDto, LoginUserDto, UpdateUserDto, UserDto } from 'src/modules/users/users.dto';
import { Role } from 'src/models/role.entity';
import { hashPassword } from 'src/shared/hashPassword';
import { SubjectService } from '../subjects/subjects.service';
import { Subject } from 'src/models/subject.entity';

const RELATIONS = ['role', 'subjects', 'lessons', 'lessons.subject', 'lessons.classData'];

@Injectable()
export class UsersService {
    constructor(
        @Inject(forwardRef(() => SubjectService))
        private readonly subjectService: SubjectService,
        @InjectRepository(User)
        private usersRepository: Repository<User>,
        @InjectRepository(Role)
        private roleRepository: Repository<Role>,
    ) {}

    async getRole(roleId: number): Promise<Role> {
        const role: Role = await this.roleRepository.findOne(roleId);

        if (!role) {
            throw new HttpException('Role does not exists', HttpStatus.BAD_REQUEST);
        }

        return role;
    }

    async create(userDto: CreateUserDto): Promise<UserDto> {
        const userInDB = await this.usersRepository.findOne({ where: { email: userDto.email } });

        if (userInDB) {
            throw new HttpException('Пользователь уже существует', HttpStatus.BAD_REQUEST);
        }

        const { role: roleId, subjects: subjectIds, ...userData } = userDto;
        const role = await this.getRole(roleId);
        const subjects = subjectIds.length ? await this.subjectService.findAll({
            where: subjectIds.map(id => ({ id }))
        }) : [];

        const user: User = this.usersRepository.create({
            ...userData,
            role,
            subjects,
        });
        await this.usersRepository.save(user);
        return toUserDto(user);
    }

    async findAll(condition?: FindManyOptions<User>): Promise<User[]> {
        const findOptions = Object.assign({ relations: RELATIONS, }, condition || {});
        return await this.usersRepository.find(findOptions);
    }

    async findOne(options?: object): Promise<UserDto> {
        const finalOptions = Object.assign(
            { relations: RELATIONS },
            options || {},
        );
        const user = await this.usersRepository.findOne(finalOptions);

        if (!user) {
            throw new HttpException('Пользователь не найден', HttpStatus.NOT_FOUND);
        }

        return toUserDto(user);
    }

    async findByEmail({ email, password }: LoginUserDto): Promise<UserDto> {
        const user = await this.usersRepository.findOne({
            where: { email },
            relations: ['role'],
            select: ['id', 'email', 'name', 'surname', 'patronymic', 'role', 'phone', 'password'],
        });

        if (!user) {
            throw new HttpException('Пользователь не найден', HttpStatus.UNAUTHORIZED);
        }

        const arePasswordsEqual = await compare(password, user.password);

        if (!arePasswordsEqual) {
            throw new HttpException('Неверный пароль', HttpStatus.UNAUTHORIZED);    
        }

        return toUserDto(user);
    }

    async findByPayload({ email }: any): Promise<UserDto> {
        return await this.findOne({ where: { email } });
    }

    async remove(id: number): Promise<void> {
        await this.usersRepository.delete(id);
    }

    async update(id: number, userDto: UpdateUserDto) {
        let additionalProperties: { role?: Role, password?: string, subjects?: Subject[] } = {};
        let { role: roleId, subjects: subjectsIds, password, ...userData } = userDto;

        if (userData.email) {
            const userInDB = await this.usersRepository.findOne({ where: { email: userData.email } });

            if (userInDB) {
                throw new HttpException('Пользователь с такой почтой уже есть в системе', HttpStatus.BAD_REQUEST);
            }    
        }

        if (roleId) {
            additionalProperties.role = await this.getRole(roleId);
        }

        if (password) {
            additionalProperties.password = await hashPassword(password);
        }

        if (subjectsIds) {
            additionalProperties.subjects = subjectsIds.length ? await this.subjectService.findAll({
                where: subjectsIds.map(id => ({ id })),
            }) : [];
        }

        const oldUser = await this.findOne({ where: { id } });
        const newUser = Object.assign(oldUser, userData, additionalProperties);
        await this.usersRepository.save(newUser);
        return newUser;
    }
}
