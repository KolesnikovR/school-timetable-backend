import {
    IsNotEmpty,
    IsEmail,
    IsPhoneNumber,
    IsString,
    IsNumber,
    IsOptional,
    IsArray,
    ArrayUnique,
} from 'class-validator';
import { Lesson } from 'src/models/lesson.entity';
import { Role } from 'src/models/role.entity';
import { Subject } from 'src/models/subject.entity';

export class CreateUserDto {  
    @IsNotEmpty() @IsString() name: string;
    @IsNotEmpty() @IsString() surname: string;
    @IsNotEmpty() @IsString() patronymic: string;
    @IsOptional() @IsString() password: string;
    @IsOptional() @IsNumber() role: number;
    @IsNotEmpty() @IsPhoneNumber() phone: string;
    @IsNotEmpty() @IsEmail() email: string;
    @IsOptional() @IsArray() @ArrayUnique() subjects: number[];
}

export class LoginUserDto {  
    @IsNotEmpty() @IsEmail() readonly email: string;
    @IsNotEmpty() @IsString() readonly password: string;
}

export class UpdateUserDto {
    @IsOptional() @IsString() name?: string;
    @IsOptional() @IsString() surname?: string;
    @IsOptional() @IsString() patronymic?: string;
    @IsOptional() @IsEmail() email?: string;
    @IsOptional() @IsString() password?: string;
    @IsOptional() @IsPhoneNumber() phone?: string;
    @IsOptional() @IsNumber() role?: number;
    @IsOptional() @IsArray() @ArrayUnique() subjects?: number[];
}

export class UserDto {
    @IsNotEmpty()  id: number;
    @IsNotEmpty()  name: string;
    @IsNotEmpty()  surname: string;
    @IsNotEmpty()  patronymic: string;
    @IsNotEmpty()  @IsEmail()  email: string;
    @IsNotEmpty()  @IsPhoneNumber()  phone: string;
    @IsNotEmpty()  role: Role;
    @IsNotEmpty()  subjects: Subject[];
    @IsNotEmpty()  lessons: Lesson[];
}

export class UserJwtPayload {
    @IsNotEmpty() @IsEmail() email: string;
}