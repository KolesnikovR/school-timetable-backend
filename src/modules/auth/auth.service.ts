import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { MailerService } from '@nestjs-modules/mailer';
import { UsersService } from 'src/modules/users/users.service';
import { CreateUserDto, LoginUserDto, UserDto } from 'src/modules/users/users.dto';
import { JwtPayload } from 'src/modules/auth/jwt.strategy';
import { Response } from 'src/shared/interfaces/response';
import { configService } from 'src/configuration/configuration.service';

export interface Token {
    expiresIn: number;
    accessToken: string;
}

export interface AuthStatus extends Response {  
    token?: Token;
    user?: UserDto;
}

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService, private readonly jwtService: JwtService,
        private readonly mailerService: MailerService,
    ) {}

    async register(userDto: CreateUserDto): Promise<AuthStatus> {
        let status: AuthStatus = {
            success: true,
        };

        try {
            const user = await this.usersService.create({
                ...userDto,
                role: userDto.role || 2,
                password: userDto.password || '12345',
            });
            status.user = user;
            try {
                await this.mailerService.sendMail({
                    to: user.email,
                    subject: 'Регистрация',
                    template: 'registration',
                    context: {
                        fullName: `${user.surname} ${user.name} ${user.patronymic}`,
                        registrationLink: `${configService.getFrontEndLink()}?token=${this._createSmallToken(user).accessToken}`,
                    },
                });   
            } catch (err) {
                console.log(err);
            }
        } catch (err) {
            status = { success: false, message: err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже" };
        }

        return status;
    }

    async login(loginUserDto: LoginUserDto): Promise<AuthStatus> {
        const user = await this.usersService.findByEmail(loginUserDto);

        return {
            success: true,
            token: this._createToken(user),    
        }; 
    }

    async validateUser(payload: JwtPayload): Promise<UserDto> {
        const user = await this.usersService.findByPayload(payload);

        if (!user) {
            throw new HttpException('Неверный токен', HttpStatus.UNAUTHORIZED);
        }

        return user;
    }

    private _createToken({ id, email, name, surname, patronymic, role, phone }: UserDto): any {
        const user: JwtPayload = { id, email, name, surname, patronymic, phone, role: role.id };
        const accessToken = this.jwtService.sign(user);    
        return {
            expiresIn: configService.getTokenLifetime(),
            accessToken,
        };  
    }

    private _createSmallToken({ email }: UserDto): any {
        const user = { email };
        const accessToken = this.jwtService.sign(user);    
        return {
            expiresIn: configService.getTokenLifetime(),
            accessToken,
        };  
    }
}
