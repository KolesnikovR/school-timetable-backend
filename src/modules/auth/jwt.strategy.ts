import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { configService } from 'src/configuration/configuration.service';
import { UserDto } from 'src/modules/users/users.dto';
import { AuthService } from 'src/modules/auth/auth.service';
import { Role } from 'src/models/role.entity';

export interface JwtPayload {
    id: number;
    email: string;
    name: string;
    surname: string;
    patronymic: string;
    phone: string;
    role: number;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(@Inject('AuthService') private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.getSecret(),
        });
    }

    async validate(payload: JwtPayload): Promise<UserDto> {
        const user = await this.authService.validateUser(payload);

        if (!user) {
            throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
        }

        return user;
    }
}
