import { Body, Controller, HttpException, HttpStatus, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateUserDto, LoginUserDto } from 'src/modules/users/users.dto';
import { AuthService, AuthStatus } from 'src/modules/auth/auth.service';
import { Roles } from 'src/decorators/roles.decorator';
import { RolesGuard } from 'src/guards/roles.guard';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('register')
    @UseGuards(AuthGuard('jwt'), RolesGuard)
    @Roles('admin')
    public async register(@Body() createUserDto: CreateUserDto): Promise<AuthStatus> {
        const result: AuthStatus = await this.authService.register(createUserDto);

        if (!result.success) {
            throw new HttpException(result.message, HttpStatus.BAD_REQUEST);
        }

        return result;
    }

    @Post('login')
    public async login(@Body() loginUserDto: LoginUserDto): Promise<AuthStatus> {
        return await this.authService.login(loginUserDto);
    }
}
