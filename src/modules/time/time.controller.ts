import { Controller, Get, HttpException, HttpStatus } from '@nestjs/common';

enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY ,
    FRIDAY,
}

const lessons = [
    { startTime: '08:00', endTime: '08:45' },
    { startTime: '09:00', endTime: '09:45' },
    { startTime: '10:05', endTime: '10:50' },
    { startTime: '11:05', endTime: '11:50' },
    { startTime: '12:05', endTime: '12:50' },
    { startTime: '13:00', endTime: '13:45' },
    // { startTime: '14:00', endTime: '14:45' },
    // { startTime: '15:00', endTime: '15:45' },
    // { startTime: '16:00', endTime: '16:45' },
    // { startTime: '16:55', endTime: '17:40' },
    // { startTime: '17:50', endTime: '18:35' },
    // { startTime: '18:45', endTime: '19:30' },
];

@Controller('time')
export class TimeController {
    constructor() {}

    @Get()
    findAll() {
        try {
            const data = Object.values(DayOfWeek)
                .filter(it => !Number.isNaN(+it))
                .map(dayOfWeek => lessons.map(lesson => ({
                    dayOfWeek,
                    ...lesson,
                }))
            );
            return [].concat.apply([], data);
        } catch(err) {
            throw new HttpException(err.getResponse ? err.getResponse() : "Внутренняя ошибка, попробуйте позже", HttpStatus.BAD_REQUEST);    
        }
    }
}
