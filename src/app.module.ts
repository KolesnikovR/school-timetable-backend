import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailerModule } from '@nestjs-modules/mailer';  
import { AppController } from 'src/app.controller';
import { AppService } from 'src/app.service';
import { configService } from 'src/configuration/configuration.service';
import { UsersModule } from 'src/modules/users/users.module';
import { AuthModule } from 'src/modules/auth/auth.module';
import { SubjectsModule } from 'src/modules/subjects/subjects.module';
import { ClassesModule } from 'src/modules/classes/classes.module';
import { LessonsModule } from 'src/modules/lessons/lessons.module';
import { TimeModule } from 'src/modules/time/time.module';

@Module({
	imports: [
		TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
		MailerModule.forRoot(configService.getSmtpConfig()),
		AuthModule,
		UsersModule,
		SubjectsModule,
		ClassesModule,
		LessonsModule,
		TimeModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {
	static port: number;

	constructor() {
		AppModule.port = parseInt(configService.getPort());
	}
}
