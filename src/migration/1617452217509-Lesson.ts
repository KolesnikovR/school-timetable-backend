import {MigrationInterface, QueryRunner} from "typeorm";

export class AddLessonTable1617452217509 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS "lesson" (
                "lesson_id" SERIAL PRIMARY KEY,
                "count_per_week" INT NOT NULL,
                "class_id" INT NOT NULL,
                "subject_id" INT NOT NULL,
                "teacher_id" INT NOT NULL,
                CONSTRAINT "fk_lesson_class"
                    FOREIGN KEY(class_id) REFERENCES class(class_id) ON DELETE CASCADE,
                CONSTRAINT "fk_lesson_subject"
                    FOREIGN KEY(subject_id) REFERENCES subject(subject_id) ON DELETE CASCADE,
                CONSTRAINT "fk_lesson_teacher"
                    FOREIGN KEY(teacher_id) REFERENCES "user"(user_id) ON DELETE CASCADE
            );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE IF EXIST "lesson"`);
    }

}
