import {MigrationInterface, QueryRunner} from "typeorm";

export class User1616603236350 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS "user" (
                "user_id" SERIAL PRIMARY KEY,
                "role_id" INT NOT NULL,
                "name" VARCHAR NOT NULL,
                "surname" VARCHAR NOT NULL,
                "patronymic" VARCHAR NOT NULL,
                "email" VARCHAR NOT NULL,
                "password" VARCHAR NOT NULL,
                "phone" VARCHAR NOT NULL,
                CONSTRAINT "fk_role"
                    FOREIGN KEY(role_id) REFERENCES role(role_id) ON DELETE CASCADE
            );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE IF EXISTS "user";`)
    }

}
