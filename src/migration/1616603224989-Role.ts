import {MigrationInterface, QueryRunner} from "typeorm";

export class Role1616603224989 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS "role" (
                "role_id" SERIAL PRIMARY KEY,
                "role_name" VARCHAR NOT NULL
            );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE IF EXISTS "role";`)
    }

}
