import {MigrationInterface, QueryRunner} from "typeorm";

export class Subject1616603457320 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS "subject" (
                "subject_id" SERIAL PRIMARY KEY,
                "subject_name" VARCHAR UNIQUE NOT NULL,
                "difficulty" VARCHAR INT NOT NULL,
            );
        `);

        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS "subject_teacher" (
                "subject_id" INT REFERENCES subject(subject_id) ON DELETE CASCADE,
                "teacher_id" INT REFERENCES "user"(user_id) ON DELETE CASCADE,
                CONSTRAINT subject_teacher_pkey PRIMARY KEY (subject_id, teacher_id)
            );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE IF EXISTS "subject_teacher";`)
        await queryRunner.query(`DROP TABLE IF EXISTS "subject";`)
    }

}
