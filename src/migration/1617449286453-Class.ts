import {MigrationInterface, QueryRunner} from "typeorm";

export class Class1617449286453 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE IF NOT EXISTS "class" (
                "class_id" SERIAL PRIMARY KEY,
                "class_name" VARCHAR UNIQUE NOT NULL
            );
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE IF EXISTS "class"`);
    }

}
